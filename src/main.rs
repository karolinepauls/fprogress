extern crate clap;
use std::io::BufReader;
use std::io::prelude::*;
use std::fs::{read_dir, canonicalize, metadata, File};
use std::path::{Path, PathBuf};
use clap::{Arg, App};
use std::process::exit;


#[derive(Debug)]
struct FileOffset {
    path: PathBuf,
    offset: Option<u64>,
}


/// Given a /proc/<pid>/fdinfo/<fdno> file, read the offset from it.
/// Returns Err if IO errors happen. Returns Ok(None) if parsing fails. Returns Ok(Some<u64>)
/// if parsing succeeds.
fn read_pos_from_fdinfo(fdinfo_path: &Path) -> std::io::Result<Option<u64>> {
    let fdinfo = BufReader::new(File::open(fdinfo_path)?);

    for line in fdinfo.lines() {
        let text = line?;
        let mut tokens = text.split_whitespace();
        match (tokens.next(), tokens.next()) {
            (Some(label), Some(value)) => {
                if label == "pos:" {
                    return Ok(value.parse::<u64>().ok());
                }
            }
            (_, _) => {}
        }
    }

    Ok(None)
}


/// Returns a vector of process' open FD paths and their current offsets.
fn get_needle_descriptor_offsets(
    pid_path: &Path,
    needle_path: &Path,
) -> std::io::Result<Vec<FileOffset>> {
    let mut fd_path = pid_path.to_path_buf();
    let mut fdinfo_path = pid_path.to_path_buf();
    fd_path.push("fd");
    fdinfo_path.push("fdinfo");
    let mut offsets = vec![];

    for fd in read_dir(fd_path)? {
        let path = fd?.path();
        if let Ok(link) = path.read_link() {
            if link == needle_path {
                {
                    // OK, since `file_name` it only fails when the name is ".." or "/".
                    let fd_name = path.file_name().unwrap();
                    fdinfo_path.push(fd_name);
                }
                if let Ok(offset) = read_pos_from_fdinfo(&fdinfo_path) {
                    offsets.push(FileOffset { path, offset });
                }
                fdinfo_path.pop();
            }
        }
    }

    Ok(offsets)
}


fn find_file_users_offests_in_procfs(needle_path: &Path) -> std::io::Result<Vec<FileOffset>> {
    let mut offsets = vec![];

    for entry in read_dir(Path::new("/proc"))? {
        if let Some(procfs_entry) = entry.map(|e| e.path()).ok() {

            let entry_pid = procfs_entry.file_name().and_then(|name| {
                name.to_string_lossy().parse::<u32>().ok()
            });

            // We care about /proc/<PID> paths, not e.g. /proc/cgroup, so the file name has to be
            // numeric.
            if entry_pid.is_some() {
                // Ignore permission errors and race conditions due to terminated processes.
                if let Ok(vec) = get_needle_descriptor_offsets(&procfs_entry, needle_path) {
                    offsets.extend(vec)
                }
            }
        }
    }

    Ok(offsets)
}

fn find_file_users_progress(file_name: &str) -> std::io::Result<(u64, Vec<FileOffset>)> {
    let needle_path = canonicalize(Path::new(file_name))?;
    let size = metadata(&needle_path)?.len();
    let file_offsets = find_file_users_offests_in_procfs(&needle_path)?;
    Ok((size, file_offsets))
}

fn main() {
    let matches = App::new("fprogress")
        .about("Displays readers' and writers' offsets within a file.")
        .arg(
            Arg::with_name("FILE")
                .help("File to examine")
                .required(true)
                .index(1),
        )
        .get_matches();

    let file_name = matches.value_of("FILE").unwrap();
    match find_file_users_progress(file_name) {
        Err(err) => {
            eprintln!("IO Error: {:?}", err.kind());
            exit(1)
        }
        Ok((file_size, file_offsets)) => {
            for p in file_offsets {
                let FileOffset { path, offset } = p;
                if let Some(offset) = offset {
                    let percentage = offset as f64 / file_size as f64 * 100.0;
                    println!(
                        "{:?}:\t{} / {} bytes\t{:8.4}%",
                        path,
                        offset,
                        file_size,
                        percentage
                    );
                } else {
                    eprintln!("{:?}: Could not read file offset from procfs.", path);
                }
            }
        }
    }
}
